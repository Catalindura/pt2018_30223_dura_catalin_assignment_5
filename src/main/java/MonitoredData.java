import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;

public class MonitoredData {
	private DateTime startTime;
	private DateTime endTime;
	private String name;
	
	
	public MonitoredData(DateTime st,DateTime et,String n) {
		startTime=st;
		endTime=et;
		name=n;
	}
	public MonitoredData() {
		
	}
	public int getZi() {
		return startTime.getDayOfMonth();
	}
	
	public int timpIntreDouaDati() {
		Date inceput=startTime.toDate();
		Date sfarsit=endTime.toDate();
		long result = sfarsit.getTime() - inceput.getTime();
		TimeUnit timeUnit=TimeUnit.MINUTES;
		return (int) timeUnit.convert(result,TimeUnit.MILLISECONDS);
	}
	
	public int timpIntreDouaDatiSecunde() {
		Date inceput=startTime.toDate();
		Date sfarsit=endTime.toDate();
		long result = sfarsit.getTime() - inceput.getTime();
		TimeUnit timeUnit=TimeUnit.SECONDS;
		int x= (int) timeUnit.convert(result,TimeUnit.MILLISECONDS);
		if (x>=300) return 0;
		else return 1;
	}
	
	public DateTime conversie(int minute) {
		int zile=minute/1440;
	    zile=zile%31;
		int ore=minute/60;
		ore=ore%24;
		int minutee=minute%60;
		DateTime x=new DateTime(0,1,zile,ore,minutee);
		return x;
	}
	
	public String toString() {
		return startTime.getYear()+"-"+startTime.getMonthOfYear()+"-"+startTime.getDayOfMonth()+" "+startTime.getHourOfDay()+":"+
				startTime.getMinuteOfHour()+":"+startTime.getSecondOfMinute()+"  "+
				endTime.getYear()+"-"+endTime.getMonthOfYear()+"-"+endTime.getDayOfMonth()+" "+endTime.getHourOfDay()+":"+
				endTime.getMinuteOfHour()+":"+endTime.getSecondOfMinute()+"  "+name;
	}
	public String getName() {
		return name;
	}
	public DateTime getStartTime() {
		return startTime;
	}
	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}
	public DateTime getEndTime() {
		return endTime;
	}
	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
