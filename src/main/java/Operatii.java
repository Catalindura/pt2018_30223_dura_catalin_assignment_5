import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
public class Operatii {
	
	public List<MonitoredData> getData() throws IOException{
			List<MonitoredData> data=new ArrayList<MonitoredData>();
			List<String> lista=new ArrayList<String>();
			String fileName = "activitati.txt";

			try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
				lista=stream.collect(Collectors.toList());
			data=lista
					.stream()
					.map(x->{
						String[] vect=x.split("\\s+");
						DateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
						String date1=vect[0]+" "+vect[1];
						String date2=vect[2]+" "+vect[3];
						DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
						DateTime myDate1 = dtf.parseDateTime(date1);
						DateTime myDate2 = dtf.parseDateTime(date2);
						MonitoredData dataa=new MonitoredData(myDate1,myDate2,vect[4]);
						return dataa;
					})
					.collect(Collectors.toList());
			return data;
		}
	}
	
	public Map<String,Integer> aparitii(List<MonitoredData> data){
		
		Map<String, Integer> collect =data.stream()
				.collect(Collectors.toMap(MonitoredData::getName, w-> 1, Integer::sum));
		return collect;

	}
	
	public Map<Integer,Map<String,Integer>> aparitiiPeZi(List<MonitoredData> data){
		Map<Integer, Map<String, Integer>> lista=data
				.stream()
				.collect(Collectors.groupingBy(MonitoredData::getZi,
						Collectors.toMap(MonitoredData::getName,w->1,Integer::sum)));
				
				
				return lista;
		}	
	public Map<String,DateTime> totalPerActivitate(List<MonitoredData> data){
		Map<String,Integer> lista1=data
				.stream()
				.collect(Collectors.groupingBy(MonitoredData::getName, Collectors.summingInt(MonitoredData::timpIntreDouaDati)));
		
		Map<String,Integer> lista2=lista1.entrySet().stream()
				.filter(x->x.getValue()>600)
				.collect(Collectors.toMap(Map.Entry::getKey,x->x.getValue()));
		
		
		Map<String,DateTime> result=lista2.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey,x->{
						MonitoredData z=new MonitoredData();
						DateTime rez=z.conversie(x.getValue());
						return rez;
				}));
		return result;
	}
	
	 public long zile(List<MonitoredData> data) {
		 
		 List<Integer> listaa=data.stream().map(x->x.getZi()).collect(Collectors.toList());
		 Stream<Integer> i=listaa.stream().distinct();
		 return i.count();
	 }
	 
	 public List<String> filtrareDurata(List<MonitoredData> data){
		 List<String> result=new ArrayList<String>();
		 
		 Map<String,DoubleSummaryStatistics> lista = data
			        .stream()
			        .collect(Collectors.groupingBy(MonitoredData::getName,Collectors.summarizingDouble(MonitoredData::timpIntreDouaDatiSecunde)));
		 
		 lista=lista.entrySet().stream()
				 .filter(x->x.getValue().getAverage()>0.9)
					.collect(Collectors.toMap(Map.Entry::getKey,x->x.getValue()));
		
		result=lista.entrySet().stream()
				.map(x->x.getKey())
				.collect(Collectors.toList());
		
		  return result;
	 }
	 
	public static void main(String args[]) throws ParseException, IOException {
		Operatii x=new Operatii();
		List<MonitoredData> data=x.getData();
		MonitoredData x1=data.get(1);
		
		
		Map<String,Integer> numarActivitati=x.aparitii(data);
		Map<Integer,Map<String,Integer>> numarActivitatiPeZi=x.aparitiiPeZi(data);
		Map<String,DateTime> ActivitatiDurata=x.totalPerActivitate(data);
		List<String> filtrare=x.filtrareDurata(data);
		
		x.filtrareDurata(data);
		FileWriter file=null;
		try {
			file = new FileWriter("OperatiiActivitati.txt");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		BufferedWriter buffer= new BufferedWriter(file);
		buffer.write("Numar de zile:"+x.zile(data));
		buffer.newLine();
		buffer.write(numarActivitati.toString());
		buffer.newLine();
		buffer.newLine();
		for (Map.Entry<Integer, Map<String,Integer>> entry : numarActivitatiPeZi.entrySet()) {
			
		buffer.write(entry.getKey()+"---"+entry.getValue());
		buffer.newLine();
}
		
		for (Map.Entry<String,DateTime> entry : ActivitatiDurata.entrySet()) {
			
			buffer.write(entry.getKey()+"---"+"Zile:"+entry.getValue().getDayOfMonth()+" Ore:"+entry.getValue().getHourOfDay()+" Minute: "+entry.getValue().getMinuteOfHour());
			buffer.newLine();
	}
		
		buffer.newLine();
		buffer.write("Activitat care au peste 90% din durata sub 5 minute:");
		for(String l:filtrare) {
			buffer.write("-"+l);
			buffer.newLine();
		}
		buffer.close();
		file.close();
	}
}
